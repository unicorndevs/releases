@echo off
setlocal
set PATH=%~dp0;%PATH%

set USERNAME=unicorn-devmaster
set REPONAME=releases
rem for %%I in (.) do set REPONAME=%%~nxI

set REPOPATH=unicorndevs/%REPONAME%
set FILEPATH=%~1
set FILENAME=%~nx1
set CURL_PARAMS=-# %2 %3 %4 %5 %6 %7 %8 %9

set STDOUT=%TEMP%\upload-file-stdout.%RANDOM%.tmp
set OUTFMT=%OUTFMT%HTTP Response Code: %%{http_code}\n
set OUTFMT=%OUTFMT%Upload Speed: %%{speed_upload} bps\n
set OUTFMT=%OUTFMT%Time Ellapsed: %%{time_total}s\n


if "%FILENAME%x" EQU "x" (
  echo Usage: %~nx0 FILE_NAME [CURL_PARAMS]
  goto :end
)


echo REPOPATH: %REPOPATH%
echo FILENAME: %FILENAME%
echo CURL_PARAMS: %CURL_PARAMS%

curl -o "%STDOUT%" -w "%OUTFMT%" -u "%USERNAME%" -X POST -k "https://api.bitbucket.org/2.0/repositories/%REPOPATH%/downloads" -F "files=@%FILEPATH%" %CURL_PARAMS%
type "%STDOUT%" && del /f /q "%STDOUT%"

:end
echo %cmdcmdline% | findstr /i /c:"%~0" >nul 2>&1 && pause
